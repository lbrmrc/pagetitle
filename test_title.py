import pytest
from title import get_title

def test_google():
    # arrange
    url = "https://www.google.com"
    # act
    result = get_title(url)
    # assert
    assert result == "Google"

def test_bing():
    # arrange
    url = "https://www.bing.com"
    # act
    result = get_title(url)
    # assert
    assert result == "Bing"

def test_invalid_url():
    # arrange
    url = "https://abc.invalid"
    # act
    with pytest.raises(Exception) as excinfo:
      get_title(url)
    # assert
    assert str(excinfo.typename) == "ConnectionError"